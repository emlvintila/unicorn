import Vue from 'vue';
import App from './App.vue';
import {dom, library} from '@fortawesome/fontawesome-svg-core';
import {faBars, faBookmark, faComment, faEye, faHeart, faPlus, faUsers} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';

Vue.config.productionTip = false;

dom.watch() // This will kick of the initial replacement of i to svg tags and configure a MutationObserver

library.add(faHeart, faComment, faBookmark, faPlus, faEye, faUsers, faBars);

Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  render: h => h(App),
}).$mount('#app');
