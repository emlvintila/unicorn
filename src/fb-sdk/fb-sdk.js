// noinspection JSUnresolvedVariable, JSUnresolvedFunction
export default class FacebookSdk {
    /**
     * @type {FacebookSdk}
     */
    static Instance = null;

    /**
     * @param {String} appId
     * @param {String} version
     */
    constructor(appId, version = 'v8.0') {
        this.appId = appId;
        this.version = version;
    }

    static get EVENT_LOGIN() {
        return 'Facebook-Login'
    }

    /**
     * @param appId
     * @param version
     * @returns {Promise<void>}
     */
    static init(appId, version) {
        if (FacebookSdk.Instance !== null) {
            return Promise.resolve();
        }

        window.FacebookSdk = FacebookSdk;
        FacebookSdk.Instance = new FacebookSdk(appId, version);
        const $this = FacebookSdk.Instance;

        let resolve;
        const promise = new Promise(res => resolve = res);

        window.fbAsyncInit = function () {
            window.FB.init({
                appId: $this.appId,
                cookie: true,
                xfbml: true,
                version: $this.version
            });

            window.FB.AppEvents.logPageView();

            $this.isInitialized = true;
            resolve();
        };

        (function (d, s, id) {
            if (d.getElementById(id))
                return;

            const fjs = d.getElementsByTagName(s)[0];
            const js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        })(document, "script", "facebook-jssdk");

        return promise;
    }

    triggerLoginEvent() {
        console.log('dispatching login event');
        document.dispatchEvent(new Event(FacebookSdk.EVENT_LOGIN));
    }

    /**
     * @returns {Promise<'connected'|'unknown'|'not_authorized'>}
     */
    getLoginStatus() {
        if (FacebookSdk.Instance === null)
            return Promise.reject();

        return new Promise(resolve => {
            // noinspection JSCheckFunctionSignatures, JSIgnoredPromiseFromCall
            window.FB.getLoginStatus(response => {
                resolve(response.status);
            });
        });
    }

    logout() {
        const $this = this;
        return new Promise(resolve => {
            // noinspection JSIgnoredPromiseFromCall
            window.FB.logout(() => {
                $this.triggerLoginEvent();
                resolve();
            });
        });
    }

    getAuthResponse() {
        return window.FB.getAuthResponse();
    }

    /**
     * @param {String} url
     * @returns {Promise<Object>}
     */
    api(url) {
        return new Promise(resolve => {
            // noinspection JSIgnoredPromiseFromCall,JSCheckFunctionSignatures
            window.FB.api(url, response => resolve(response));
        })
    }
}
